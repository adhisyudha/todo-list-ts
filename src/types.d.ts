type Todo = {
  id: number,
  text: string,
  complete: boolean
}

type ToogleTodo = (selectedItem: Todo) => void;

type AddTodo = (newTodos:string) => void;