import React, { useState } from 'react';
import { TodoList } from './TodoList';
import { AddTodoForm } from './AddTodoForm';

const initialTodos: Array<Todo> = [
  {
    id: 1,
    text: "learn coding",
    complete: true
  },
  {
    id: 2,
    text: "learn coding2",
    complete: false
  },
]

const initEdit = {
  id: 0,
  text: "",
  complete: false
}

const App: React.FC = () => {
  const [todos, setTodos] = useState(initialTodos)
  const [editTodo, setEditTodo] = useState(initEdit)
  const [isAction, setIsAction] = useState('add')
  const toogleTodo:ToogleTodo = (selectedTodo) => {
    let objIndex = todos.findIndex((obj => obj.id === selectedTodo.id));
    todos[objIndex].complete = !selectedTodo.complete;
    todos[objIndex].text = selectedTodo.text
    // Object.assign(todos[objIndex], selectedTodo);
    setTodos([...todos]);
    
    // const newTodos = todos.map(todo => {
    //   console.log('todo',todo);
    //   if (todo === selectedTodo) {
    //     return {
    //       ...todo,
    //       complete: !todo.complete
    //     };
    //   }
    //   return todo
    // });
    // setTodos(newTodos)
  }

  const deleteItem:ToogleTodo = (selectedTodo) => {
    let temp = todos.filter(function(item) {
      return item.id !== selectedTodo.id
    })
    setTodos(temp);
  }

  const editItem:ToogleTodo = (selectedTodo) => {
    setEditTodo(selectedTodo);
    setIsAction('edit');
    // console.log('editItem:ToogleTodo', selectedTodo);
  }
  console.log('isAction', isAction);

  const handleEdit:ToogleTodo = (selectedTodo) =>{
    let objIndex = todos.findIndex((obj => obj.id === selectedTodo.id));
    todos[objIndex].text = selectedTodo.text;
    todos[objIndex].complete = selectedTodo.complete;
    setTodos([...todos]);
    setIsAction('add');
  }
 
  const addTodo:AddTodo = (newTodos) => {
    (newTodos.trim() !== '') && 
      setTodos([...todos, {id: new Date().getTime() ,text:newTodos, complete:false}])
  }

  return (
    <React.Fragment>
      <TodoList 
        todos={todos} 
        toogleTodo={toogleTodo} 
        deleteItem={deleteItem}
        editItem={editItem}
      />
      <AddTodoForm 
        addTodo={addTodo} 
        editTodo={editTodo}
        editItem={handleEdit}
        isAction={isAction}
      />
    </React.Fragment>
  );
}

export default App;
