import React from "react";
import "./TodoListItem.css";

interface TodoListItemProps {
    todo : Todo,
    toogleTodo: ToogleTodo,
    deleteItem: ToogleTodo,
    editItem: ToogleTodo
}
export const TodoListItem: React.FC<TodoListItemProps> = ({ todo, toogleTodo, deleteItem, editItem }) => {
    return(
        <li>
            <label className={todo.complete ? "complete" : undefined}>
                <input 
                    type="checkbox" 
                    checked={todo.complete}
                    onChange={() => toogleTodo(todo)} 
                /> {todo.id} - {todo.text} - {todo.complete ? 'true':'false'} 
                <button onClick={() => deleteItem(todo)}>delete</button>
                <button onClick={() => editItem(todo)} >edit</button>
            </label>
        </li>
    ) 
}