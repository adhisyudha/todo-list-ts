import React, { useState, ChangeEvent, FormEvent, useEffect } from "react"

interface AddTodoFormProps {
    addTodo: AddTodo,
    editTodo: {
        id: number,
        text: string,
        complete: boolean
    },
    editItem: ToogleTodo,
    isAction: String
}

export const AddTodoForm: React.FC<AddTodoFormProps> = ({addTodo, editTodo, editItem, isAction}) => {
    const [newTodo, setNewTodo] =  useState("");
    const handleChange = (e:ChangeEvent<HTMLInputElement>) => {
        setNewTodo(e.target.value)
    }

    const handleSubmit = (e:FormEvent<HTMLButtonElement>) => {
        e.preventDefault();
        
        if (isAction !== 'add') {            
            //do edit
            editTodo.text = newTodo;
            editItem(editTodo)
            setNewTodo("");
                   
        }else{
            //do create
            addTodo(newTodo);
            setNewTodo("");
        }
    }
    
    useEffect(()=>{     
        if (isAction === 'edit') {
            setNewTodo(editTodo.text);
        }
    },[editTodo, isAction])
    
    return (
        <form action="">
            <input type="text" name="" id="" value={newTodo} onChange={handleChange} />
            <button type="submit" onClick={handleSubmit}>Add</button>
        </form>
    )
}