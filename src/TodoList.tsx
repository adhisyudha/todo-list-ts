import React from "react"
import { TodoListItem } from "./TodoListItem"

interface TodoListProps {
    todos: Array<Todo>,
    toogleTodo: ToogleTodo,
    deleteItem: ToogleTodo,
    editItem: ToogleTodo
}

export const TodoList:React.FC<TodoListProps> = ({todos, toogleTodo, deleteItem, editItem}) => {
    return(
        <ul>
            {todos.map(todo=>{
                return <TodoListItem key={todo.id} todo={todo} toogleTodo={toogleTodo} deleteItem={deleteItem} editItem={editItem} />
            })}
        </ul>
    )
}